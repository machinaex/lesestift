Config
-------

### special_chars

If the first character matches a special_char, the word is interpreted in a specific way

* never

do not make this word audible.

* always

always make this word audible no matter what length etc.

### never

List of words that, once read, should not be audible

### always

List of words that should always be audible

### complexity

define rules to interpret words differently depending on the length

* fragmented

number of characters from which the word will be read as single letters

* fail

number of characters from which the word will not be read

* interval

time between reading single characters

### endurance

Reader deactivates or makes noise depending on how much it is used.

* words

number of words until exhaustion

* pause

time in seconds until reader can read again



Raspberry Pi einrichten
-----------------------

### asyncio installieren

pip3 install pyserial-asyncio

### numpy installieren

sudo apt install python3-numpy

### Image erstellen

siehe auch: https://www.raspberrypi.org/documentation/installation/installing-images/linux.md
Mit disk alle partitionen bis auf eine löschen. Ggf. formatieren in FAT

Dann image aufspielen, z.B.:

`$ sudo dd bs=4M if=~/Downloads/2019-06-20-raspbian-buster-full.img of=/dev/sda status=progress conv=fsync`


### Picotts installieren

nerdvittles.com/?p=16463

```
cd /
wget http://incrediblepbx.com/picotts-raspi.tar.gz
tar zxvf picotts-raspi.tar.gz
rm -f picotts-raspi.tar.gz
cd /root
echo "Installing Pico TTS..."
./picotts-install.sh
```

ggf. alles mit sudo
shell wirft fehler, scheinta aber trotzdem zu funktionieren


### Sound konfigurieren

`$ sudo amixer cset numid=3 1`


### Autostart einrichten

In Einstellungen die Raspberry Config anpassen und Boot to Desktop deaktivieren bzw. umschalten auf boot to CLI.

die Datei .bashrc modifizieren:

sudo nano /home/pi/.bashrc

Am Ende der Datei anfügen:

`$ echo Start Text To Speech`
`$ sudo python /home/pi/Documents/sign_here/lesestift/code-to-speech.py`


### Ordner synchronisieren

rsync nutzen um lesestift repo ordner auf pi mit lokaler version zu synchronisieren z.B.:

`$ sudo rsync -avz -e ssh ~/Repos/sign_here/lesestift/ pi@192.168.0.103:Documents/sign_here/lesestift`
