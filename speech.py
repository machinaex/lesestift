#
# say words with picotts and aplay using subprocess
#
# picotts languages 'de-DE', 'en-GB', 'en-US', 'es-ES', 'fr-FR', 'it-IT'
#

import subprocess, json, time

class Speech:
    def __init__(self):
        with open('config.json', 'r') as f:
            self.config = json.load(f)
            
        self.letters = []
    
    #
    # call on word start character
    #
    def start(self):
        self.letters = []
    
    #
    # collect single letters.
    #
    def append(self, letter):
        # self.letters.append(letter)
        self.say(letter)
    
    #
    # call on word end character
    #
    # condition: was the word read correctly
    #
    def complete(self, word, condition):
        self.letters = []
        if condition:
            appendix = "!"
        else:
            appendix = "?"
            
        self.interpret(word+appendix)
    
    #
    #
    #
    def word(self, word):
        self.interpret(word)

    def interpret(self, word):
        if word[:2] == self.config["special_chars"]["never"]:
            #play("fail.wav")
            print("Can't say the word")
            return
            
        if word[:2] == self.config["special_chars"]["always"]:
            self.say(word)
            return
        
        if word[:2] == self.config["special_chars"]["request"]:
            self.say(word[2:])
            return
            
        if len(word) > self.config["complexity"]["fragmented"]:
            print("this will be fragmented because its too long")
            self.divide(word, self.config["complexity"]["interval"])
            return
            
        self.say(word)

    def divide(self, word, interval):
        for letter in word:
            self.say(letter)
            time.sleep(interval)


    def say(self, text):
        # self.picotts_exe(["--wave","tts.wav",text + "."])
        self.picotts_exe(["--wave","tts.wav",text])
        self.play("tts.wav")
    
    def play(self, file):
        p = subprocess.Popen(["aplay", file],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
        res = iter(p.stdout.readline, b'')
        res2 = []
        for line in res:
            res2.append(line)
        return res2
        
    def picotts_exe(self, args, sync=True):
            cmd = ['pico2wave', 
                   '-l', 'de-DE', 
                   ]

            cmd.extend(args)

            p = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT)
            res = iter(p.stdout.readline, b'')
            if not sync:
                return res

            res2 = []
            for line in res:
                res2.append(line)
            return res2

if __name__ == "__main__":
    
    speech = Speech()
    
    speech.say("Hallo. Benutze das input skript um die Anwendung zu testen.")
