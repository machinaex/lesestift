import asyncio
import serial_asyncio
import serial.tools.list_ports
import numpy as np

import sys

import binascii
import string
import speech

talk = speech.Speech()

_common = (
    ' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',',
    '-', '.', '/',
) + tuple(string.digits) + (
    ':', ';', '<', '=', '>', '?', '@',
) + tuple(string.ascii_uppercase) + (
    '[', '\\', ']', '^', '_',
)

charset_A = _common + (
    '\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08',
    '\x09', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e', '\x0f', '\x10', '\x11',
    '\x12', '\x13', '\x14', '\x15', '\x16', '\x17', '\x18', '\x19', '\x1a',
    '\x1b', '\x1c', '\x1d', '\x1e', '\x1f', '\xf3', '\xf2', 'SHIFT', 'TO_C',
    'TO_B', '\xf4', '\xf1',
)

charset_B = _common + ('`',) + tuple(string.ascii_lowercase) + (
    '{', '|', '}', '~', '\x7f', '\xf3', '\xf2', 'SHIFT', 'TO_C', '\xf4',
    'TO_A', '\xf1','START_A','START_B','START_C','STOP','RSTOP'
)

charset_Y = tuple(' ') + \
            tuple((' ') * 15) + tuple(string.digits) +  \
            tuple((' ') * 7) + tuple(string.ascii_uppercase) + \
            tuple((' ') * 6) + tuple(string.ascii_lowercase) + \
            tuple((' ') * 17)

codesNp = np.array([
   [2,1,2,2,2,2],[2,2,2,1,2,2],[2,2,2,2,2,1],[1,2,1,2,2,3], 
   [1,2,1,3,2,2],[1,3,1,2,2,2],[1,2,2,2,1,3],[1,2,2,3,1,2], 
   [1,3,2,2,1,2],[2,2,1,2,1,3],[2,2,1,3,1,2],[2,3,1,2,1,2], 
   [1,1,2,2,3,2],[1,2,2,1,3,2],[1,2,2,2,3,1],[1,1,3,2,2,2],
   [1,2,3,1,2,2],[1,2,3,2,2,1],[2,2,3,2,1,1],[2,2,1,1,3,2],
   [2,2,1,2,3,1],[2,1,3,2,1,2],[2,2,3,1,1,2],[3,1,2,1,3,1], 
   [3,1,1,2,2,2],[3,2,1,1,2,2],[3,2,1,2,2,1],[3,1,2,2,1,2],
   [3,2,2,1,1,2],[3,2,2,2,1,1],[2,1,2,1,2,3],[2,1,2,3,2,1], 
   [2,3,2,1,2,1],[1,1,1,3,2,3],[1,3,1,1,2,3],[1,3,1,3,2,1],
   [1,1,2,3,1,3],[1,3,2,1,1,3],[1,3,2,3,1,1],[2,1,1,3,1,3],
   [2,3,1,1,1,3],[2,3,1,3,1,1],[1,1,2,1,3,3],[1,1,2,3,3,1],
   [1,3,2,1,3,1],[1,1,3,1,2,3],[1,1,3,3,2,1],[1,3,3,1,2,1],
   [3,1,3,1,2,1],[2,1,1,3,3,1],[2,3,1,1,3,1],[2,1,3,1,1,3], 
   [2,1,3,3,1,1],[2,1,3,1,3,1],[3,1,1,1,2,3],[3,1,1,3,2,1],
   [3,3,1,1,2,1],[3,1,2,1,1,3],[3,1,2,3,1,1],[3,3,2,1,1,1],
   [3,1,4,1,1,1],[2,2,1,4,1,1],[4,3,1,1,1,1],[1,1,1,2,2,4],
   [1,1,1,4,2,2],[1,2,1,1,2,4],[1,2,1,4,2,1],[1,4,1,1,2,2],
   [1,4,1,2,2,1],[1,1,2,2,1,4],[1,1,2,4,1,2],[1,2,2,1,1,4], 
   [1,2,2,4,1,1],[1,4,2,1,1,2],[1,4,2,2,1,1],[2,4,1,2,1,1],
   [2,2,1,1,1,4],[4,1,3,1,1,1],[2,4,1,1,1,2],[1,3,4,1,1,1],
   [1,1,1,2,4,2],[1,2,1,1,4,2],[1,2,1,2,4,1],[1,1,4,2,1,2],
   [1,2,4,1,1,2],[1,2,4,2,1,1],[4,1,1,2,1,2],[4,2,1,1,1,2],
   [4,2,1,2,1,1],[2,1,2,1,4,1],[2,1,4,1,2,1],[4,1,2,1,2,1], 
   [1,1,1,1,4,3],[1,1,1,3,4,1],[1,3,1,1,4,1],[1,1,4,1,1,3], 
   [1,1,4,3,1,1],[4,1,1,1,1,3],[4,1,1,3,1,1],[1,1,3,1,4,1],
   [1,1,4,1,3,1],[3,1,1,1,4,1],[4,1,1,1,3,1],[2,1,1,4,1,2],
   [2,1,1,2,1,4],[2,1,1,2,3,2],[2,3,3,1,1,1],[2,1,1,1,3,3]
])

class BarcodeReader:
    def __init__(self):
        self.count = 0
        self.times = [0,0,0,0,0,0]
        self.codeString = []
        self.numsString = []
        self.start = 0
        self.stop = 0
        self.idxString = [[],[],[],[],[],[]]
        self.normString = [[],[],[],[],[],[]]
        self.sumWeight = [4,4,4,4,4,4]

    def append(self, time):
        self.times.append(time)
        self.times.pop(0)
        self.count+=1
        
        times_arr = np.array(self.times)
        
        sym=np.divide(times_arr, np.average(times_arr)*6/11)
        sym_s=np.around(sym).astype(np.uint8).tobytes()

        #Calculate the eucledian norm of every symbol in respect of the found times
        norm = np.linalg.norm(codesNp-np.array(sym), axis=1)
        char_idx = np.argmin(norm)
        
        #If the norm to every symbol is bigger than 3 we assume the barcode
        #is read completly. 
        if norm[char_idx] > 3 and len(self.normString[self.count%6]) > 3:
            
            #find out which of the 6 possible permutations performed best
            best_idx = self.sumWeight.index(min(self.sumWeight))
            
            #start (104) suchen, stop (106) suchen, checksumme
            
            checksumOk = False
            
            if 104 in self.idxString[best_idx] and \
                106 in self.idxString[best_idx]:
                
                idxStringWithoutSTOP = np.array(self.idxString[best_idx][1:-2])
                mult = np.multiply(idxStringWithoutSTOP, np.arange(1,idxStringWithoutSTOP.size+1))
                checksum = np.fmod((np.sum(mult)+104), 103)
                
                if self.idxString[best_idx][-2] == checksum:
                    # Checksumme passt
                    #
                    checksumOk = True
                    print(" CHECKSUM OK ",end='')
            
            completeString = ""        
            for c in self.idxString[best_idx][1:-2]:
                completeString += charset_Y[c]   
            
            # vollständiges "Wort"
            #
            print(f" COMPLETE {completeString}")         
            talk.complete(completeString, checksumOk)
        
        if norm[char_idx] > 3:
            self.idxString = [[],[],[],[],[],[]]
            self.normString = [[],[],[],[],[],[]]
        
        self.idxString[self.count%6].append(char_idx)
        self.normString[self.count%6].append(norm[char_idx])
        
        self.sumWeight[self.count%6] = sum(self.normString[self.count%6][1:])
        
        if self.count%6 == 0:
            # aktuell gelesener Buchstabe
            #
            readChar = charset_Y[self.idxString[self.sumWeight.index(min(self.sumWeight))][-1]]
            talk.append(readChar)
            
            #having read a complete char:
            print(readChar,end='',flush=True)


class Output(asyncio.Protocol):

    def connection_made(self, transport):
        self.transport = transport
        self.lastInput = np.array([0,0,0,0,0,0])
        self.datas = bytearray()
        self.lastT=0
        self.lastInput = np.array([0,0,0,0,0,0])
        self.lastInputIndex = 0
        self.plotCode = []
        self.ourCode = []
        print('port opened', transport)
        
        self.loop = asyncio.get_event_loop()
        self.loop.add_reader(sys.stdin, self.key_press)
        
        self.barcodeReader = BarcodeReader()
        

    def data_received(self, data):
        self.datas.extend(data)
        while len(self.datas) >= 4:
            t = int.from_bytes(self.datas[0:3], byteorder='little', signed=False)
            self.datas = self.datas[4:]
            tdiff=t-self.lastT
            self.lastT=t
            
            self.plotCode.append(tdiff)
            self.barcodeReader.append(tdiff)


            
    def key_press(self):
         sys.stdin.readline()
         print(self.barcodeReader.charString)
         print(self.barcodeReader.normString)
         #plt.plot(self.plotCode)
         #plt.show()
            

    def connection_lost(self, exc):
        print('port closed')
        asyncio.get_event_loop().stop()

loop = asyncio.get_event_loop()
coro = serial_asyncio.create_serial_connection(loop, Output, '/dev/ttyACM0', baudrate=57600)
loop.run_until_complete(coro)
loop.run_forever()
loop.close()
